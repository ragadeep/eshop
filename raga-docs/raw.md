Install GitLab Runner
https://docs.gitlab.com/runner/install/linux-manually.html
For runner register
https://docs.gitlab.com/runner/register/

sast:
  stage: test
  tags:
    - build
    - microservices
include:
  - template: Auto-DevOps.gitlab-ci.yml

-------------
Install docker in runner
sudo apt install docker.io -y

-----------------
Docker login -u $ACRUSER -p $ACRPassowd $DOCKER Resistory
Sudo usermod -aG docker gitlab-runner

root@GitLabRunner:~# gpasswd -a $USER docker
Adding user root to group docker


---------
Delete images
docker ps -aq --filter "status=exited" | xargs docker rm  # Remove all stopped containers
docker rmi -f $(docker images -q)  # Force delete all Docker images

--------
Install in runner for currency build

sudo apt update
sudo apt install python3 make g++

--------------
Update Run command in currency & Payment docker file if u use ubuntu runner
#Update package index and install packages
RUN apk update && \
    apk add --no-cache python3 make g++

------------------
See docker registred in config.toml for image scane
[[runners]]
  name = "My Runner"
  url = "https://gitlab.com/"
  token = "XXXXXXXX"
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "docker:latest"
    privileged = true #Required for accessing the Docker socket
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache:/cache"]     # Mount Docker socket


-------------
Project name eshop (put this name only)
------------
Adding login server ip in the runner
ssh-keyscan -H 13.71.64.229 >> ~/.ssh/known_hosts
-----------------------
TO run all files at once
for file in *.yaml; do
  kubectl apply -f "$file"
done

--------------------------
TO give sudo permission to raga

For Debian-based systems 
sudo apt update
sudo apt install sudo

Open the sudoers file using the visudo command, which provides a safe way to edit the file:
	- sudo visudo
Add the following line to the end of the file to grant sudo privileges to a user named raga:
	- raga    ALL=(ALL:ALL) ALL

----
After adding it will give error prompting so again change it 

	- sudo visudo

Change which added prevously
	- raga ALL=(ALL) NOPASSWD: /home/raga/kube/acr_secret.sh


-------------------------------------
Path will be different for root and normal user

$ which kubectl
/home/raga/.local/bin/kubectl
$ sudo su - root
$ which kubectl
/usr/local/bin/kubectl


-------------------------------------

nohup socat TCP-LISTEN:31250,fork TCP:192.168.49.2:31250 > /dev/null 2>&1 &

ps aux | grep socat

------------------------------------------------------------

      imagePullSecrets:
      - name: acr-secret


------------------------------------------------------------