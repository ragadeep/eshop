Step by step process to Set up GitLab-ee managed Instance on linux ubuntu server

- sudo apt update

- sudo apt install -y curl openssh-server ca-certificates postfix

- PoPup comes:   
    - Internet site          #select internal site when pop-up appears
    - Server DNS 

- curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

- sudo apt install gitlab-ee

- sudo nano /etc/gitlab/gitlab.rb
    - external_url '<server_DNS>'

- sudo gitlab-ctl reconfigure

Now access Gitlab-ee in you browser using server DNS


#cmd to get root password
sudo cat /etc/gitlab/initial_root_password

USERNAME : root
PASSWORD :        #get password from about command
