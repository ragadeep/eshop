Step by step process to Set up Sonarqube in your linux ubuntu server

- sudo apt update
- sudo apt install docker.io -y  #To install latest docker

- docker run -d --name sonar -p 9000:9000 sonarqube:lts-community  (https://hub.docker.com/_/sonarqube)
    - Username: admin
    - Passeword: admin

Login page

![Alt text](img/sonar_login_page.png)


After Login Select manual project

![Alt text](img/sonar_login2.png)


Click on -> with GitLab CI

![Alt text](img/sonarqube3.png)


Step 1:
Select set project key based on your code
In my case I am choosing other

![Alt text](img/sonarqube4.png)


Create file in you GitLab Repository
Paste the mentioned code in that file

![Alt text](img/sonarqube5.png)
Then click on continue


Step 2:
Follow the below steps mentioned in snapshot

![Alt text](img/sonarqube6.png)


Step 3:
Add the script in GitLab-ci.yml pipeline

![Alt text](img/sonarqube7.png)


Once Pipeline Successful then you can open sonar browser (serverip:sonarport)

![Alt text](img/sonarqube8.png)
