This Project I have cloned from https://github.com/nanuchi/microservices-demo
And main Project orginated from Google https://github.com/GoogleCloudPlatform/microservices-demo

This Readme I have added for my understanding. Official Readme you can find in Readme2.md

Overall website looks like 

![Alt text](<raga-docs/img/Online Boutique.png>)

![Alt text](<raga-docs/img/Online Boutique2.png>)

Here is the Architecture flow for Present Workflow

![Alt text](<raga-docs/img/Present Architecture.png>)




Below is the Architecture flow Planned for Future implementaions.

![Alt text](raga-docs/img/Future_Implementation.png)





- Firstly I have Create GitLab managed instance in Azure Server. You can find step to this in GitLab.md file

- I have used Sonarcode Quality Scan also which you can find in Sonalqube in Sonarqube.md file

- I have deployed code on minikube cluseter which i created in my ubuntu server. Find file in Kubernetes.md

- Variables.md file defines all the variable which I used in this project





